class Student {
  String stuID = 'NULL';
  String stuName = 'NULL';
  String stuSurName = 'NULL';
  bool giveBirthday = true;
  String stuBirthday = 'NULL';

  //Parameterized Constructor
  Student(stuID, stuName, stuSurName, stuBirthday);

  //Named Constructor
  Student.withoutBirthday(stuID, stuName, stuSurName) : giveBirthday = false;
}

void main() {
  Student s1 = Student('123', 'John', 'Smith', '1 Jan 2000');
  Student s2 = Student.withoutBirthday('111', 'Jim', 'Null');
}
