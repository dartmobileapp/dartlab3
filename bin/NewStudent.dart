class NewStudent {
  NewStudent.constructor1(int a) {
    print(
        ' This is the parameterized constructor with only one parameter : $a ');
  }

  NewStudent.constructor2(int a, int b) {
    print(' This is the parameterized constructor with two parameters ');
    print(' Value of a + b is ${a + b} ');
  }
}

void main() {
  // creating two objects s1 and s2 of class student

  NewStudent s1 = NewStudent.constructor1(5);

  NewStudent s2 = NewStudent.constructor2(7, 8);
}
