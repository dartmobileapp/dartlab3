class Calculator {
  double? a;
  double? b;

  calculator(double a, double b) {
    this.a = a;
    this.b = b;
  }

  double add(double a, double b) {
    return a + b;
  }

  double minus(double a, double b) {
    return a + b;
  }

  double multiple(double a, double b) {
    return a * b;
  }

  double divide(double a, double b) {
    if (b == 0) {
      print("Errors : Input b must more than 0 !!");
      return 0;
    } else {
      return a / b;
    }
  }
}
