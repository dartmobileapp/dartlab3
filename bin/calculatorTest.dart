import 'dart:io';
import 'Calculator.dart';

void main(List<String> args) {
  int? menu;
  Calculator cal = new Calculator();
  print('MENU');
  print('Select the choice you want to perform : ');
  print('1.ADD');
  print('2.MINUS');
  print('3.MULTIPLE');
  print('4.DIVIDE');
  print('5.EXIT');

  print("Choice you want to enter : ");
  menu = int.parse(stdin.readLineSync()!);

  switch (menu) {
    case 1:
      {
        print("Enter the value for x : ");
        double a = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double b = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.add(a, b));
      }
      break;
    case 2:
      {
        print("Enter the value for x : ");
        double a = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double b = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.minus(a, b));
      }
      break;
    case 3:
      {
        print("Enter the value for x : ");
        double a = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double b = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.multiple(a, b));
      }
      break;
    case 4:
      {
        print("Enter the value for x : ");
        double a = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double b = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.divide(a, b));
      }
      break;
    case 5:
      break;
  }
}
